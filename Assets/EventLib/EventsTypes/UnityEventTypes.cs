﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace EventsLib.EventTypes
{

    [System.Serializable]
    public class UnityEventTransform : UnityEvent<Transform> { }

    [System.Serializable]
    public class UnityEventVector3 : UnityEvent<Vector3> { }
}
