﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using EventsLib.EventTypes;

[System.Serializable]
public class BallEventos
{
    public UnityEvent onBallExplode;
}

[RequireComponent(typeof(Rigidbody),typeof(SphereCollider))]
public class BallBehavior : MonoBehaviour
{
    [SerializeField] BallEventos eventos;
    [SerializeField] float startSpeed;
    [SerializeField] float speedMultiply;
    [SerializeField] float maxVelocity;
    [SerializeField] float delayToExplode;
    float actualSpeed;
    Rigidbody c_rigidBody;
    SphereCollider c_sphereCollider;
    bool alive;

    private void Start()
    {
        c_rigidBody = GetComponent<Rigidbody>();
        c_sphereCollider = GetComponent<SphereCollider>();
        actualSpeed = startSpeed;
        alive = true;
    }

    public void BallDeflect(Vector3 _directionToGo)
    {
        if (!alive)
            return;

        if (actualSpeed * speedMultiply > maxVelocity)
        {
            eventos.onBallExplode.Invoke();
            alive = false;
            return;
        }

        actualSpeed *= speedMultiply;

        c_rigidBody.velocity = _directionToGo * actualSpeed;
    }

    public void BallLaunch(Vector3 _directionToGo)
    {
        if (!alive)
            return;

        UnParent();
        c_rigidBody.velocity = _directionToGo * actualSpeed;
    }

    public void MakeParent(Transform t)
    {
        if (!alive)
            return;

        c_sphereCollider.enabled = false;
        c_rigidBody.velocity = Vector3.zero;
        c_rigidBody.isKinematic = true;
        transform.parent = t;
        transform.position = t.position;
    }

    void UnParent()
    {
        c_sphereCollider.enabled = true;
        c_rigidBody.isKinematic = false;
        transform.parent = null;
    }

    public void ICantGetThisAnymore()
    {
        StartCoroutine(Timer_BallExplode());
    }

    IEnumerator Timer_BallExplode()
    {
        float totalTime = 0;

        while(totalTime + Time.deltaTime <= delayToExplode)
        {
            totalTime += Time.deltaTime;

            //TODO: Feedback bola explodindo
            transform.localScale *= (totalTime / delayToExplode) - 1;

            yield return new WaitForEndOfFrame();
        }

        Destroy(gameObject);
    }
}
