﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAimBehavior : MonoBehaviour
{
    [SerializeField] LayerMask layerToCastAim;

    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000000, layerToCastAim.value))
        {
            Vector3 a = new Vector3(hit.point.x, 1, hit.point.z);
            transform.LookAt(new Vector3(hit.point.x, 1, hit.point.z));
        }
    }
}
