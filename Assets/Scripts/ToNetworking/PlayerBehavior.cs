﻿using EventsLib.EventTypes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Eventos
{
    public UnityEventVector3 onAttack;
    public UnityEventVector3 onCounterBeat;
    public UnityEventTransform onTakeBall;
}

[RequireComponent(typeof(Rigidbody))]
public class PlayerBehavior : MonoBehaviour
{
    [SerializeField] Eventos eventos;
    [SerializeField] float timeToLetHerGo;
    [SerializeField] float speed;
    [SerializeField] float speedDodge;
    [SerializeField] float dodgeMaxDistance;
    [SerializeField] Transform ballPlaceHolder;
    [SerializeField] bool mds;
    Vector3 initialPosition;
    Vector3 aimPoint;
    bool isDodging;
    Coroutine timer_ToLauchTheBall;
    Coroutine doDodge;

    //Componentes
    Rigidbody c_rigidBody;
    GameObject actualBall;

    // Start is called before the first frame update
    void Start()
    {
        c_rigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();

        if (Input.GetKeyDown(KeyCode.C))
            mds = !mds;

        if (Input.GetMouseButtonDown(1))
        {
            Dodge();
        }

        if (Input.GetButtonDown("Attack"))
        {
            if (actualBall)
            {
                StopCoroutine(timer_ToLauchTheBall);                
                eventos.onAttack.Invoke(transform.forward);
            }
        }
    }

    void Move()
    {
        if (!isDodging)
        {
            c_rigidBody.velocity = new Vector3(
                Input.GetAxisRaw("Horizontal"),
                c_rigidBody.velocity.y,
                Input.GetAxisRaw("Vertical")) * speed;
        }
    }

    void Dodge()
    {

        if (!isDodging)
        {
            Vector3 target = transform.position + transform.forward * dodgeMaxDistance;
            initialPosition = transform.position;
            isDodging = true;
            target.y = 1;
            doDodge = StartCoroutine(DoDodge(transform.position, target));
        }
    }

    IEnumerator DoDodge(Vector3 _startPosition, Vector3 _target)
    {
        Vector3 direction = _target - _startPosition;

        c_rigidBody.velocity = direction.normalized * speedDodge;
        c_rigidBody.velocity = new Vector3(c_rigidBody.velocity.x, 0, c_rigidBody.velocity.z);

        Vector3 predictMove = transform.position + c_rigidBody.velocity * Time.deltaTime;

        while (Vector3.Distance(_startPosition, predictMove) <= dodgeMaxDistance)
        {
            yield return new WaitForFixedUpdate();
            predictMove = transform.position + c_rigidBody.velocity * Time.deltaTime;
        }

        c_rigidBody.velocity = Vector3.zero;
        isDodging = false;
    }

    IEnumerator Timer_ToLauchTheBall()
    {
        yield return new WaitForSeconds(timeToLetHerGo);
        
        
        eventos.onAttack.Invoke(transform.forward);
    }

    private void OnTriggerExit(Collider other)
    {
        if (actualBall != null && other.gameObject != actualBall.gameObject)
            return;

        actualBall = null;
    }

    private void OnTriggerStay(Collider other)
    {
        if (!(other.gameObject.CompareTag("Ball")))
            return;

        if (actualBall)
            return;

        if (Input.GetButtonDown("Attack"))
            eventos.onCounterBeat.Invoke(transform.forward);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Terrain"))
        {
            StopCoroutine(doDodge);
            c_rigidBody.velocity = Vector3.zero;
            isDodging = false;
        }

        if (!(collision.gameObject.CompareTag("Ball")))
            return;

        if (actualBall)
            return;

        if (mds)
            return;

        

        timer_ToLauchTheBall = StartCoroutine(Timer_ToLauchTheBall());
        actualBall = collision.transform.gameObject;
        eventos.onTakeBall.Invoke(ballPlaceHolder);
    }
}