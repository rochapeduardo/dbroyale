﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] float speed;
    Rigidbody rb;
    Collider ballCollider;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        ballCollider = GetComponent<Collider>();
    }

    public void BallMove(Vector3 dir)
    {
        ballCollider.enabled = true;
        rb.isKinematic = false;
        transform.parent = null;
        
        rb.velocity = Vector3.zero;
        rb.AddForce(dir * speed, ForceMode.Impulse);

    }

    public void MakeParent(Transform t)
    {
        ballCollider.enabled = false;
        rb.velocity = Vector3.zero;
        transform.parent = t;
        rb.isKinematic = true;
    }
}
