﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] int index;
    [SerializeField] float dodgeForce;
    [SerializeField] float dodgeDistance;
    Vector3 initialPosition;
    Vector3 aimPoint;
    bool isDodging;

    //Componentes
    Rigidbody rb;

    Ball ball;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Aim();
        Move();
        if (ball)
        {
            if (Input.GetButtonDown("Attack" + index.ToString()))
            {
                ball.BallMove(ball.transform.position - transform.position);
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            Dodge((aimPoint-transform.position).normalized);
        }
        if (isDodging)
        {
            if (Vector3.Distance(initialPosition, transform.position) > dodgeDistance)
            {
                isDodging = false;
            }
        }
    }

    void Move()
    {
        if (!isDodging)
        {
            float _x = Input.GetAxisRaw("Horizontal" + index);
            float _y = Input.GetAxisRaw("Vertical" + index);
            Vector3 _velocity = new Vector3(_x, rb.velocity.y, _y) * speed * Time.deltaTime;
            rb.velocity = _velocity; 
        }
    }

    void Dodge(Vector3 dir)
    {
        if (aimPoint != Vector3.zero)
        {
            dir = new Vector3(dir.x, 0, dir.z);
            if (!isDodging)
            {
                initialPosition = transform.position;
                rb.AddForce(dir * dodgeForce, ForceMode.Impulse);
                isDodging = true;
            }
        }
    }

    void Aim()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            if (hit.transform.CompareTag("Terrain"))
                aimPoint = hit.point;
            else
                aimPoint = Vector3.zero;
            print(hit.transform.name);
        }
        else
            aimPoint = Vector3.zero;
    }

    private void OnTriggerExit(Collider other)
    {
        if (ball != null)
            if (other.gameObject == ball.gameObject)
                ball = null;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            if(ball == null)
            {
                if (Input.GetButtonDown("Attack" + index.ToString()))
                {

                    other.GetComponent<Ball>().BallMove(other.transform.position - transform.position);

                }
            }
            
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Ball"))
            return;
        if (ball!=null && ball.gameObject == collision.gameObject)
            return;

        ball = collision.transform.GetComponent<Ball>();
        ball.MakeParent(transform);
    }
}
